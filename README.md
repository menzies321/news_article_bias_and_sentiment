Political Bias Classifier and Sentiment Analysis

This project outlines a prototype model for using TF-IDF Vectorization, Support Vector Machines, and VADER sentiment analysis to predict the bias present in political news article text. Please find attached two Jupyter Notebook files to follow along, as well as documents outlining a brief overview and the process for WebCrawling and running the Natural Language Processing model. A PowerPoint with audio files is also provided for a brief rundown.

note: The file full_data.pkl is the result of the first Jupyter Notebook, and can be used to import the entire harvested data dataset at the beginning of the second Jupyter Notebook.